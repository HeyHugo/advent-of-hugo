package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"strconv"
	"time"
)

// ProgramI defines a program
type ProgramI interface {
	set(byte, int)
	add(byte, int)
	mul(byte, int)
	mod(byte, int)
	jgz(byte, int)
	rcv(byte)
	snd(byte)
	nextOp() (string, byte, int)
}

// Instruction to be used by Programs
type Instruction struct {
	operation string
	r         byte
	value     string
}

// Program that can follow a set of instructions
type Program struct {
	instructions []Instruction
	index        int
	register     map[byte]int
}

func (p *Program) set(r byte, value int) {
	p.register[r] = value
}

func (p *Program) add(r byte, value int) {
	p.register[r] += value
}

func (p *Program) mul(r byte, value int) {
	p.register[r] *= value
}

func (p *Program) mod(r byte, value int) {
	p.register[r] = p.register[r] % value
}

func (p *Program) jgz(r byte, value int) {
	leftValue, err := strconv.Atoi(string(r))
	if err != nil {
		leftValue = p.register[r]
	}
	if leftValue > 0 {
		p.index += value - 1
	}
}

func (p *Program) nextOp() (string, byte, int) {
	i := p.instructions[p.index]
	var value int
	var err error

	if i.value != "" {
		value, err = strconv.Atoi(i.value)
		if err != nil {
			value = p.register[[]byte(i.value)[0]]
		}
	}
	p.index++
	return i.operation, i.r, value
}

// MusicProgram that can follow a set of instructions to make a sound
type MusicProgram struct {
	*Program
	sound              int
	recoveredFrequency chan int
}

func (p *MusicProgram) snd(r byte) {
	p.sound = p.register[r]
}

func (p *MusicProgram) rcv(r byte) {
	if p.register[r] != 0 {
		p.recoveredFrequency <- p.sound
	}
}

// MsgProgram that can follow a set of instructions to communicate with another program
type MsgProgram struct {
	*Program
	inBuffer  chan int
	outBuffer chan int
	sendCount int
	pid       int
	done      chan bool
}

func newMsgProgram(
	pid int,
	instructions []Instruction,
	inBuffer chan int,
	outBuffer chan int) *MsgProgram {
	return &MsgProgram{
		pid:  pid,
		done: make(chan bool),
		Program: &Program{
			instructions: instructions,
			register:     map[byte]int{byte('p'): pid},
		},
		inBuffer:  inBuffer,
		outBuffer: outBuffer,
	}
}

func (p *MsgProgram) snd(r byte) {
	if p.pid == 1 {
		p.sendCount++
	}
	p.outBuffer <- p.register[r]
}

func (p *MsgProgram) rcv(r byte) {
	// Identify deadlock (programs waiting for each other)
	if len(p.inBuffer) == 0 && len(p.outBuffer) == 0 && p.sendCount > 1 {
		p.done <- true
	}

	select {
	case p.register[r] = <-p.inBuffer:
	}
}

func run(p ProgramI) {
	for {
		operation, v1, v2 := p.nextOp()

		switch operation {
		case "set":
			p.set(v1, v2)
		case "add":
			p.add(v1, v2)
		case "mul":
			p.mul(v1, v2)
		case "mod":
			p.mod(v1, v2)
		case "jgz":
			p.jgz(v1, v2)
		case "rcv":
			p.rcv(v1)
		case "snd":
			p.snd(v1)
		}
	}
}

func main() {
	input, err := ioutil.ReadFile("./input")
	if err != nil {
		panic(err)
	}
	input = bytes.TrimRight(input, "\n")
	problem1(input)
	problem2(input)
}

func loadInstructions(input []byte) []Instruction {
	instructions := []Instruction{}
	lines := bytes.Split(input, []byte("\n"))

	for _, line := range lines {
		parts := bytes.Split(line, []byte(" "))
		i := &Instruction{operation: string(parts[0]), r: parts[1][0]}
		if len(parts) == 3 {
			i.value = string(parts[2])
		}
		instructions = append(instructions, *i)
	}

	return instructions
}

func problem1(input []byte) {
	t1 := time.Now()
	var p ProgramI = &MusicProgram{
		recoveredFrequency: make(chan int),
		Program: &Program{
			register:     make(map[byte]int),
			instructions: loadInstructions(input),
		},
	}

	go run(p)
	mp, _ := p.(*MusicProgram)
	var recoveredFrequency int
	select {
	case recoveredFrequency = <-mp.recoveredFrequency:
	}

	t2 := time.Since(t1)

	fmt.Printf("Problem1 result: %v\n", recoveredFrequency)
	fmt.Printf("Time: %v\n", t2)
}

func problem2(input []byte) {
	t1 := time.Now()
	c1, c2 := make(chan int, 100), make(chan int, 100)
	instructions := loadInstructions(input)
	p1 := newMsgProgram(0, instructions, c1, c2)
	p2 := newMsgProgram(1, instructions, c2, c1)

	go run(p1)
	go run(p2)

	select {
	case <-p2.done:
	}

	t2 := time.Since(t1)
	fmt.Printf("Problem2 result: %v\n", p2.sendCount)
	fmt.Printf("Time: %v", t2)
}
