package main

import (
	"fmt"
	"time"
)

func main() {
	input := 369
	problem1(input)
	problem2(input)
}

func problem1(input int) {
	current := 0
	buffer := []int{0}
	t1 := time.Now()
	for i := 1; i < 2018; i++ {
		temp := []int{}
		temp = append(temp, buffer[:current+1]...)
		temp = append(temp, i)
		temp = append(temp, buffer[current+1:]...)
		buffer = temp
		current = (current + input + 1) % len(buffer)
	}
	t2 := time.Since(t1)

	fmt.Printf("Result: %v\n", buffer[current-input+1])
	fmt.Printf("Time: %v\n", t2)
}

func problem2(input int) {
	t1 := time.Now()
	current := 0
	lastZeroNeighbor := 0
	for i := 1; i <= 50000000; i++ {
		current = (current+input)%i + 1
		if current == 1 {
			lastZeroNeighbor = i
		}
	}
	t2 := time.Since(t1)

	fmt.Printf("Result: %v\n", lastZeroNeighbor)
	fmt.Printf("Time: %v\n", t2)
}
